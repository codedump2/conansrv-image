#!/bin/bash

set -x
set -euo pipefail

# Exports a conan configuration file to $CONAN_SERVER_HOME
# and runs the server. Needs these variables (some defaults
# may be set). They all represent the respective config
# parameters.

# The config file location. It needs to be on CONAN_SERVER_HOME
CONSRV_CONFIG_FILE=${CONSRV_CONFIG_FILE:="$CONAN_SERVER_HOME/server.conf"}

# Set to "yes" or "Yes" or "true" or "True" or "1" to skip generation
# of a new server.conf file. If an old file is in place, that's what will
# be reused. It's mostly just a useful feature if you want to severly
# restructure the config file beyond what this script and its env vars
# are capable of doing.
CONSRV_CONFIG_REUSE=${CONSRV_CONFIG_REUSE:="No"}

# Generate a random sequence for JWT_SECRET
CONSRV_JWT_SECRET=${CONSRV_JTW_SECRET:=`dd if=/dev/urandom of=/dev/stdout bs=12 count=1 status=none | base64`}

# Default data directory: $
CONSRV_DISK_STORAGE_PATH=${CONSRV_DISK_STORAGE_PATH:="$CONAN_SERVER_HOME/data"}

# Seconds after which uploads/downloads are aborted
CONSRV_AUTHORIZE_TIMEOUT=${CONSRV_AUTHORIZE_TIMEOUT:=1000}

# For token generation for uploads/downloads (WTF?)
CONSRV_UPDOWN_SECRET=${CONSRV_UPDOWN_SECRET:=`dd if=/dev/urandom of=/dev/stdout bs=12 count=1 status=none | base64`}

# Set this to the correct port if there is port-forwarding involved.
CONSRV_PUBLIC_PORT=${CONSRV_PUBLIC_PORT:="443"}

# Set this to the publicly visible hostname
CONSRV_HOST_NAME=${CONSRV_HOST_NAME:="localhost"}

# Set this to "true" if the forward-facing HTTP proxy does SSL
CONSRV_SSL_ENABLED=${CONSRV_SSL_ENABLED:="True"}

# If this is set to non-empty, then a verbatim "user: password" is
# inserted into the config file, aside from the LDAP authentication.
CONSRV_ADD_USER=${CONSRV_ADD_USER:=""}

# Permissions are a funny thing... don't know exactly what to expect
# and whether to restrict reading/writing to specific users for specific
# projects. But for default, we'll go for: every user can read/write
# everything.
CONSRV_ADD_READ_PERMISSIONS=${CONSRV_ADD_READ_PERMISSIONS="*/*@*/*: *"}
CONSRV_ADD_WRITE_PERMISSIONS=${CONSRV_ADD_WRITED_PERMISSIONS="*/*@*/*: *"}

# If this is set, a custom ldap authenticator is added to conan.
# ACHTUNG, if you use ldap_authentication.py, the $HOME directory
# needs to point to the right location, too! It is _not_ enough
# to have $CONAN_SERVER_HOME set correctly!
CONSRV_LDAP_HOST=${CONSRV_LDAP_HOST:=""}
CONSRV_LDAP_DN=${CONSRV_LDAP_DN:=""}

# Some gunicorn config variables
CONSRV_WORKERS=${CONSRV_WORKERS:="4"}

#
# Some debugging info for funny deployment scenarii
#

echo "System mounts:"
cat /proc/mounts
echo

echo "I am: `id`"
echo

echo "And I have many secrets:"
echo "   - jwt_secret: $CONSRV_JWT_SECRET"
echo "   - updown_secret: $CONSRV_UPDOWN_SECRET"
echo

#
# Making sure the filesystem is as expected
#

ls -l /home || echo no home
ls -l /home/jinkies || echo no jinkies
ls -l /home/jinkies/.conan_server || echo no conan_server

[ ! -d $CONAN_SERVER_HOME ] \
    && mkdir -p $CONAN_SERVER_HOME

[ ! -d $CONSRV_DISK_STORAGE_PATH ] \
    && mkdir -p $CONSRV_DISK_STORAGE_PATH

#
# Setting up config
#

case "$CONSRV_CONFIG_REUSE" in
    Yes|yes|True|true|1)
	echo "Reusing $CONSRV_CONFIG_FILE:"
	;;

    *)
	cat > $CONSRV_CONFIG_FILE << EOF
[server]
jwt_secret: $CONSRV_JWT_SECRET
jwt_expire_minutes: 120

ssl_enabled: $CONSRV_SSL_ENABLED
port: 9300
public_port: $CONSRV_PUBLIC_PORT
host_name: $CONSRV_HOST_NAME

store_adapter: disk

# if this is too small, upload/download of very large files
# might get aborted!
authorize_timeout: $CONSRV_AUTHORIZE_TIMEOUT

# Just for disk storage adapter
disk_storage_path: $CONSRV_DISK_STORAGE_PATH
disk_authorize_timeout: 1800

updown_secret: $CONSRV_UPDOWN_SECRET
EOF


if [ "foo$CONSRV_LDAP_HOST" != "foo" ]; then
    echo "custom_authenticator: ldap_authentication" >> $CONSRV_CONFIG_FILE

    ## ldap_authentication.py is usually installed in the ~/.conan_server/
    ## of the user. The problem is that, at runtime, this is sometimes an
    ## externally mounted volume (e.g. in a cluster), so we won't have this
    ## available until we actually _run_ this (e.g. now). However, pip
    ## also leaves a copy in /usr/local/...site-packages.
    ## So what we're doing is looking for the file on the filesystem
    ## and copy it to where it's supposed to be.
    mkdir -p $CONAN_SERVER_HOME/plugins/authenticator/
    find /usr/local -name ldap_authentication.py -exec cp {} $CONAN_SERVER_HOME/plugins/authenticator/ \;
    ls -l $CONAN_SERVER_HOME/plugins/authenticator/
    find /usr/local -name ldap_authentication.py
fi

	cat >> $CONSRV_CONFIG_FILE <<EOF
[write_permissions]
$CONSRV_ADD_WRITE_PERMISSIONS

[read_permissions]
$CONSRV_ADD_READ_PERMISSIONS
EOF

	# add an extra user?
	if [ "foo$CONSRV_ADD_USER" != "foo" ]; then
	    cat >> $CONSRV_CONFIG_FILE <<EOF
[users]
$CONSRV_ADD_USER
EOF
	fi


	# probably not necessary
	#export CONAN_LDAP_AUTHENTICATION_CONFIG_FILE=$CONAN_SERVER_HOME/ldap_authentication.conf
	# Genrate LDAP authenticator file, if LDAP is set
	if [ "foo$CONSRV_LDAP_HOST" ]; then
	    cat > $CONAN_SERVER_HOME/ldap_authentication.conf <<EOF
[ldap]
host: $CONSRV_LDAP_HOST
distinguished_name: $CONSRV_LDAP_DN
EOF
	fi
	
	;;  ## CONSRV_CONFIG_REUSE=False
esac


##
## Show config file
##

echo "Conan Server config file:"
echo " ---cut-here---"
cat $CONSRV_CONFIG_FILE
echo " ---/cut-here---"

if [ "foo$CONSRV_LDAP_HOST" ]; then
    echo
    echo "LDAP config file for conan server:"
    echo "---cut-here---"
    cat $CONAN_SERVER_HOME/ldap_authentication.conf
    echo "---/cut-here---"
fi

#
# WhereTheMagickHappens(tm).
#
# We're setting the workers timeout to the same value as
# AUTHORIZE_TIMEOUT for conan_server (seems sensible).
#
gunicorn -b 0.0.0.0:9300 \
	 -w $CONSRV_WORKERS \
	 -t $CONSRV_AUTHORIZE_TIMEOUT \
	 --log-level debug \
	 conans.server.server_launcher:app
