FROM registry.fedoraproject.org/fedora:35

RUN \
	dnf -y update && \
	dnf -y install \
	python3-pip \
	python3-mod_wsgi \
	python3-devel \
	python-pip-wheel \
	openldap-devel \
	openssl-devel \
	findutils \
	gcc \
	git && \
	pip install conan && \
	pip install conan_ldap_authentication && \
	pip install gunicorn && \
	useradd -u 1001 jinkies -m && \
	chmod a+x /home/jinkies

ENV CONAN_SERVER_HOME=/home/jinkies/.conan_server

# Apparently, conan_server searches for authentication plugins
# (ldap_authentication.py) *not* in $CONAN_SERVER_HOME/plugins,
# but in $HOME/.conans_server/plugins...
# So we do need this in order for the server to work.
ENV HOME=/home/jinkies

COPY conan-runsrv /sbin/conan-runsrv

USER jinkies

CMD conan-runsrv

        
