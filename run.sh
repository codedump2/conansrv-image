#!/bin/bash

# Runs a local conansrv-image instance using podman
# (For debugging only)

podman run -ti --rm \
       -p 9300:9300/tcp \
       -e CONSRV_ADD_USER="jinkies: s3cr3t" \
       -e CONSRV_PUBLIC_PORT="9300" \
       -e CONSRV_PUBLIC_HOST="localhost" \
       -e CONSRV_SSL_ENABLED="False" \
       --user root \
       conansrv-image $@
